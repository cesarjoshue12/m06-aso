#!/bin/bash
# extreure els name de ldap
name=""
llista_usuaris=$(getent passwd)
for usuaris in $llista_usuaris
do
  uids=$(echo $usuaris | cut -d ':' -f3)
  if [[ $uids == 500* ]]
  then
    names=$(echo $usuaris | cut -d ':' -f1)
    name="$name $names"
  fi
done

# programa
for user in $name
do
  echo -e "$user\n$user" | smbpasswd -a $user
  uid=$(echo $line | cut -d ':' -f3)
  gid=$(echo $line | cut -d ':' -f4)
  homedir=$(echo $line | cut -d: -f4)
  echo "$user $uid $gid $homedir"
  if [ ! -d $homedir ]
  then
    mkdir -p $homedir
    cp -ra /etc/skel/. $homedir
    chown -R $uid.$gid $homedir
  fi
done


