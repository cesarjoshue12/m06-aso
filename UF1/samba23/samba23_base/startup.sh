#!/bin/bash
# @cesaraguilarmendez
# -------------------------------
# share public
mkdir -p /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

# share privat
mkdir -p /var/lib/samba/privat
cp /etc/os-release /var/lib/samba/privat/.

# copiar la configuració
cp /opt/docker/smb.conf /etc/samba/smb.conf



