#! /bin/bash

for user in unix01 unix02 unix03 unix04 unix05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done

rm -rf /etc/ldap/ldap.conf
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf

mkdir /var/www/webdav
cd /var/www/webdav
chgrp www-data /var/www/webdav
chmod g+w /var/www/webdav
git clone https://gitlab.com/edtasixm06/m06-aso.git
cd /opt/docker

a2enmod dav_fs
a2enmod auth_digest

cp apache2-webdav.conf /etc/apache2/conf-enabled

apachectl -DFOREGROUND -k start


