# Desplegar amb docker compose ldap+ssh+phpLdapAdmin
## compose.yml
```
version: "3"
services:
  ssh:
    container_name: ssh.edt.org
    hostname: ssh.edt.org
    image: cesaraguilarmendez/ssh23:base
    privileged: true
    ports:
      - "2022:22"
    networks:
      - 2hisx
  ldap:
    container_name: ldap.edt.org
    hostname: ldap.edt.org
    image: cesaraguilarmendez/ldap23:latest
    networks:
      - 2hisx
  phpldapadmin:
    container_name: phpldapadmin.edt.org
    hostname: phpldapadmin.edt.org
    image: cesaraguilarmendez/phpldapadminer
    ports:
      - 80:80
    networks:
      - 2hisx

networks:
  2hisx:
```
