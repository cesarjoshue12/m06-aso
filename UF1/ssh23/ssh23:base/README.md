# SSH BASE

## Dockerfile
Paquets necessaris:

nslcd, nslcd-utils, ldap-utils, libnss-ldapd, libpam-ldapd, davfs2, openssh-server openssh-client net-tools

```Dockerfile
FROM debian:latest
LABEL author="César Aguilar Méndez"
LABEL subject="pam23_ldap"
RUN apt-get update
RUN apt-get -y install procps iproute2 tree nmap vim less finger passwd libpam-pwquality libpam-mount libnss-ldapd libpam-ldapd nslcd nslcd-utils ldap-utils davfs2 openssh-server openssh-client net-tools
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
```

## Fitxers necessaris
**/etc/ldap/ldap.conf**: Fitxer que ens permetra realitzar la connexió de la base de dades amb el servidor ldap.
```
BASE	dc=edt,dc=org
URI	ldap://ldap.edt.org
```

### **/etc/nslcd.conf**:
```
# The location at which the LDAP server(s) should be reachable.
uri ldap://ldap.edt.org 

# The search base that will be used for all queries.
base dc=edt,dc=org

```

### **/etc/nsswitch.conf**: ordre de prioritat.
```
passwd:         files ldap
group:          files ldap
```
### **/etc/pam.d/common-session**: Fitxer configuració pam
```
# and here are more per-package modules (the "Additional" block)
session	required	pam_unix.so 
session optional	pam_mkhomedir.so
session	optional	pam_mount.so 
session	[success=ok default=ignore]	pam_ldap.so minimum_uid=1000
# end of pam-auth-update config
```


### **/etc/security/pam_mount.conf.xml**:Fitxer creació volums.
```
		<!-- Volume definitions -->
<volume
	user="*"
	fstype="tmpfs"
	mountpoint="~/mytmp"
	options="size=100M"
	/>

<volume
	uid="5000-9999"
	fstype="davfs"
	path="http://dav.edt.org/webdav"
	mountpoint="~/apunts"
	options="username=%(USER),uid=%(USERUID),gid=%(USERGID),file_mode=0770,dir_mode=0770"
	/>

		<!-- pam_mount parameters: General tunables -->
```

### startup.sh
```
#! /bin/bash

for user in unix01 unix02 unix03 unix04 unix05
do 
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done

/bin/bash

rm -rf /etc/nsswitch.conf
rm -rf /etc/nslcd.conf
rm -rf /etc/ldap/ldap.conf
rm -rf /etc/pam.d/common-session
rm -rf /etc/security/pam_mount.conf.xml
cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
cp /opt/docker/nslcd.conf /etc/nslcd.conf
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
cp /opt/docker/common-session /etc/pam.d/common-session
cp /opt/docker/pam_mount.conf.xml /etc/security/pam_mount.conf.xml
# activar dimonis
/usr/sbin/nscd 
/usr/sbin/nslcd

mkdir /run/sshd
/usr/sbin/sshd -D

```

### Desplegar container 
Amb docker run:
```
docker run --rm --name ssh.edt.org -h ssh.edt.org --net 2hisx -p 2022:22 --privileged -d cesaraguilarmendez/ssh23:base
```
```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d cesaraguilarmendez/ldap23:latest
```

# Desplegar amb docker compose

## compose.yml
```
version: '2'
services:
  ldap:
    image: cesaraguilarmendez/ldap23:latest
    container_name: ldap.edt.org
    hostname: ldap.edt.org
    ports:
      - 389:389
    networks:
      - mynet
  ssh:
    container_name: ssh.edt.org
    hostname: ssh.edt.org
    image: cesaraguilarmendez/ssh23:base
    privileged: true
    ports:
      - "2022:22"
    networks:
      - 2hisx
networks:
  2hisx:

```

### Desplegar amb compose.yml
```
docker compose up -d
```

