schema
# LDAP SCHEMA PRACTICA
## Afegir schema al slapd.conf
```
include 	/opt/docker/bambes.schema

```

## Crear schema
```
# practica.schema
#
# x-bambes:
#   x-nom
#   x-talla
#   x-imatge
#   x-especificacions
#
# x-fabricacio:
#   x-data-release
#   x-custom
#   x-origen
#-----------------------------------------------

attributetype (1.1.2.1.1.1 NAME 'x-nom'
  DESC 'Nom de la bamba'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)

attributetype (1.1.2.1.1.2 NAME 'x-talla'
  DESC 'Numero talla'
  EQUALITY integerMatch
  ORDERING integerOrderingMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.27
  SINGLE-VALUE)

attributetype (1.1.2.1.1.3 NAME 'x-imatge'
  DESC 'Imatge de la bamba'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.28)

attributetype (1.1.2.1.1.4 NAME 'x-especificacions'
  DESC 'Documentació de la bamba'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.5)

attributetype (1.1.2.2.1.1 NAME 'x-data-release'
  DESC 'Data de sortida de la bamba'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)

attributetype (1.1.2.2.1.2 NAME 'x-custom'
  DESC 'Bamba custom si/no'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.7
  SINGLE-VALUE)

attributetype (1.1.2.2.1.3 NAME 'x-origen'
  DESC 'Procedencia de la bamba'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)


objectClass (1.1.2.1.1.1 NAME 'x-bamba'
  DESC 'Calçat lleuger i comode que s utilitza els peus'
  SUP TOP
  STRUCTURAL
  MUST ( x-nom $ x-talla )
  MAY ( x-imatge $ x-especificacions ))

objectClass (1.1.2.2.1.1 NAME 'x-fabricacio'
  DESC 'Fabricacio de la bamba'
  SUP TOP
  AUXILIARY
  MUST ( x-data-release $ x-custom $ x-origen ))


```
## Objecte STRUCTURAL
```
objectClass (1.1.2.1.1.1 NAME 'x-bamba'
  DESC 'Calçat lleuger i comode que s utilitza els peus'
  SUP TOP
  STRUCTURAL
  MUST ( x-nom $ x-talla )
  MAY ( x-imatge $ x-especificacions ))

```

## Objecte AUXILIAR
```
objectClass (1.1.2.2.1.1 NAME 'x-fabricacio'
  DESC 'Fabricacio de la bamba'
  SUP TOP
  AUXILIARY
  MUST ( x-data-release $ x-custom $ x-origen ))
```

## Crear entitats

```
dn: x-nom=jordanmid 1,ou=practica,dc=edt,dc=org
objectClass: x-bamba
objectClass: x-fabricacio
x-nom: jordanmid 1
x-talla: 39
x-imatge:< file:/opt/docker/jordanmid-1.jpeg
x-especificacions:< file:/opt/docker/bamba1.pdf
x-data-release: 2-12-2005
x-custom: False
x-origen: Portugal


dn: x-nom=airforce 1,ou=practica,dc=edt,dc=org
objectClass: x-bamba
objectClass: x-fabricacio
x-nom: airforce 1
x-talla: 44
x-imatge:< file:/opt/docker/airforce-1.jpeg
x-especificacions:< file:/opt/docker/bamba2.pdf
x-data-release: 1-1-1998
x-custom: True
x-origen: Asia

dn: x-nom=dunk panda,ou=practica,dc=edt,dc=org
objectClass: x-bamba
objectClass: x-fabricacio
x-nom: dunk panda
x-talla: 41
x-imatge:< file:/opt/docker/dunk-panda.jpeg
x-especificacions:< file:/opt/docker/bamba3.pdf
x-data-release: 14-12-2003
x-custom: False
x-origen: Spain


```

## Per engegar container

- docker run --rm --name ldap -h ldap.edt.org --net 2hisx -p 389:389 -d cesaraguilarmendez/ldap23:practica

- docker run --rm  --name phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisx -p 80:80 -d edtasixm06/ldap23:phpldapadmin 

