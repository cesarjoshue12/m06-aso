# LDAP groups
## Enunciat
● modificar el fitxer edt.org.ldif per afegir una ou grups.

● definir els següents grups:
- alumnes(600)
- professors(601) 
- 1asix(610), 2asix(611),
- sudo(27)
- 1wiam(612)
- 2wiam(613)
- 1hiaw(614).

● Els grups han de ser elements posixGroup

● verificar el llistat dels usuaris i grups i la coherència de dades entre
els usuaris que ja teníem i els nous grups creats.

● Modificar el startup.sh perquè el servei ldap escolti per tots els
protocols: ldap ldaps i ldapi.

### 1) Modificar RDN perquè els usuaris s'identifiquin amb UID
Exemple:
```
dn: uid=pau,ou=usuaris,dc=edt,dc=org
```

### 2) Afegir ou=grups
```
dn: ou=grups,dc=edt,dc=org
ou: grups
description: Container per a grups
objectclass: organizationalunit
```

### 3) Crear tots els grups:

- alumnes(600):
```
dn: cn=alumnes,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: alumnes
gidNumber: 600
description: Grup de alumnes
memberUid: anna
memberUid: marta
memberUid: jordi
```
- professors(601):
```
dn: cn=professors,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: professors
gidNumber: 601
description: Grup de professors
memberUid: pau
memberUid: pere
memberUid: jordi

```

- 1asix(610):
```
dn: cn=1asix,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1asix
gidNumber: 610
description: Grup de 1asix
memberUid: user01
memberUid: user02
memberUid: user03
memberUid: user04
memberUid: user05  
```

- 2asix(611):
```
dn: cn=2asix,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 2asix
gidNumber: 611
description: Grup de 2asix
memberUid: user06
memberUid: user07
memberUid: user08
memberUid: user09
memberUid: user10
```
- sudo(27):
```
dn: cn=sudo,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: master
gidNumber: 27
description: Grup de sudo
memberUid: admin
```

- 1wiam(612):
```
dn: cn=1wiam,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1wiam
gidNumber: 612
description: Grup de 1wiam
```

- 2wiam(613):
```
dn: cn=2wiam,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 2wiam
gidNumber: 613
description: Grup de 2wiam
```

- 1hiaw(614):
```
dn: cn=1hiaw,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1hiaw
gidNumber: 614
description: Grup de 1hiaw
```

### 4) Modificar admin sigui grup sudo(27)
```dn: uid=admin,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Administrador Sistema
cn: System Admin
sn: System
homephone: 555-222-2225
mail: anna@edt.org
description: description admin
ou: system
ou: admin
uid: admin
uidNumber: 10
gidNumber: 27
homeDirectory: /tmp/home/admin
userPassword: {SSHA}4mS0FycWc5bkpW8/a396SGNDTQUlFSX3
```
### 5) Modficar startup.sh per els protocols ldap, ldaps, ldapi
```
#!/bin/bash

echo "Configurant el servidor ldap..."

#1 Esborrar els directoris de configuració i de dades
#2 Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.conf
#3 Injectar a baix nivell les dades de la BD 'populate' de l'organització dc=edt,dc=org
#4 Assignar la propietat i grup del directori de ddaes i de configuració a l'usuari openldap
#5 Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground

#1
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*
#2
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
#3 -l on està el fitxer on hi ha les dades que vull injectar
slapadd -F /etc/ldap/slapd.d/ -l /opt/docker/edt-org.ldif
slapcat
#4
chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
#5
/usr/sbin/slapd -d0 -h 'ldap:/// ldaps:/// ldapi:///'
```
### Comprovar
```
└─$ ldapsearch -xv -LLL -b 'ou=grups,dc=edt,dc=org'
ldap_initialize( <DEFAULT> )
filter: (objectclass=*)
requesting: All userApplication attributes
dn: ou=grups,dc=edt,dc=org
ou: grups
description: Container per a grups
objectClass: organizationalunit

dn: cn=sudo,ou=grups,dc=edt,dc=org
objectClass: posixGroup
cn: master
cn: sudo
gidNumber: 27
description: Grup de sudo
memberUid: admin
```

- Professors:
```
─$ ldapsearch -xv -LLL -b 'dc=edt,dc=org' 'gidNumber=601'
ldap_initialize( <DEFAULT> )
filter: gidNumber=601
requesting: All userApplication attributes
dn: uid=pau,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Pau Pou
cn: Pauet Pou
sn: Pou
homePhone: 555-222-2220
mail: pau@edt.org
description: description pau
ou: Profes
uid: pau
uidNumber: 5000
gidNumber: 601
homeDirectory: /tmp/home/pau
userPassword:: e1NTSEF9TkRraXBlc05RcVRGRGdHSmZ5cmFMei9jc1pBSWxrMi8=

dn: uid=pere,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Pere Pou
sn: Pou
homePhone: 555-222-2221
mail: pere@edt.org
description: description pere
ou: Profes
uid: pere
uidNumber: 5001
gidNumber: 601
homeDirectory: /tmp/home/pere
userPassword:: e1NTSEF9Z2htdFJMMTFZdFhvVWhJUDd6NmY3bmI4UkNOYWRGZSs=
```
- Alumnes:
```
└─$ ldapsearch -xv -LLL -b 'dc=edt,dc=org' 'gidNumber=600'
ldap_initialize( <DEFAULT> )
filter: gidNumber=600
requesting: All userApplication attributes
dn: uid=anna,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Anna Pou
cn: Anita Pou
sn: Pou
homePhone: 555-222-2222
mail: anna@edt.org
description: description anna
ou: Alumnes
uid: anna
uidNumber: 5002
gidNumber: 600
homeDirectory: /tmp/home/anna
userPassword:: e1NTSEF9Qm00QjNCdS9mdUg2QmJ5OWxneGZGQXdMWXJLMFJiT3E=

dn: uid=marta,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Marta Mas
sn: Mas
homePhone: 555-222-2223
mail: marta@edt.org
description: description marta
ou: Alumnes
uid: marta
uidNumber: 5003
gidNumber: 600
homeDirectory: /tmp/home/marta
userPassword:: e1NTSEF9OSsxRjJmNXZjVzh6L3RtU3pZTldkbHo1R2JEQ3lvT3c=
```
## Engegar container:
```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -p 636:636 cesaraguilarmendez/ldap23:groups
```


