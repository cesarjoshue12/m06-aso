# Ldap23 editat

## Afegim usuaris0-11 i nova ou

- Dins del fitxer edt-org.ldif afegim usuari0-11 i la nova ou=npc
```
dn: ou=npc,dc=edt,dc=org
ou: npc
description: Container per npc
objectclass: organizationalunit

dn: cn=usuari0,ou=npc,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: usuari0
sn: user0
uid: usuari0
uidNumber: 8000
gidNumber: 1000
homeDirectory: /tmp/home/usuari0
```

## Modificar RDN per uid

- En el meu cas creem fitxer modrdn.ldif i introduïm en la casella de newrdn: uid= usuarinª userª
```
dn: cn=usuari0,ou=npc,dc=edt,dc=org
changetype: modrdn
newrdn: uid=usuari user0
deleteoldrdn: 0
```
- La comanda per que es modifiqui el RDN des del el fitxe modrdn.ldif
```
ldapmodify -x -D 'cn=Manager,dc=edt,dc=org' -w secret -f modrdn.ldif
```
## Canviar PASSWD

```
slappasswd  -h {MD5} -s secret
{MD5}Xr4ilOzQ4PCOq3aQ0qbuaQ==
```

## Iniciar container

```
docker run --rm --name ldap23:editat -h ldap.edt.org --net 2hasix cesaraguilarmendez/ldap23:editat 
```

