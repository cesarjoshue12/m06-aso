#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# @César M06 Curs 2023-2024
# exemple-echoClient.py
# --------------------------------------
import sys,socket
HOST = '' # localhost
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Crea un objecte de tipus socket (socket.AF_INET --> internet, socket.SOCK_STREAM --> TCP)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # Quan acabem el programa deixa utilitzar el port, sino hauriem d'esperar o utilitza un altre port (TIME WAIT)
s.bind((HOST,PORT)) # Per quines inteficies i port ha de escoltar
s.listen(1) # per quin port escolta
conn, addr = s.accept() # Es queda clavat esperant a algú (s.accept--> TUPLA)
print("Conn:", type(conn), conn) # Variable que es un objecte que representa una connexió
print("Connected by:", addr)
while True:
    data = conn.recv(1024)
    if not data: break # if not data NO SIGNIFICA NO HI HA DADES --> VOL DIR SI HAN TANCAT LA CONNEXIÓ
    conn.send(data)
    print(data)
conn.close()
sys.exit(0)




