#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# @edt ASIX M06 Curs 2023-2024
# -------------------------------
import sys
MAX=5
fileIn=sys.stdin
# open -> retorna un file description, retorna un numero que ens dona informacio sobre una regio de memoria on esta emmagatzemada la info
# r = read
# sys.argv[1] -> procesa el primer argument
# sys.argv[0] -> es el propi programa
# sys.stdin -> entrada estandard (teclat)


# si li passem un argument processa l'argument, si no li passem argument, processa l'entrada estandar
if len(sys.argv)==2:
    fileIn=open(sys.argv[1],"r")
i=0 
for line in fileIn:
    i+=1
    print(line,end="")
    if i==MAX: break
fileIn.close()
exit(0)


