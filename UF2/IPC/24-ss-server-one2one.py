#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# @César Aguilar Méndez
# ss-server.py [-p port] 
# --------------------------------------
import sys, socket, argparse, os, signal
from subprocess import Popen, PIPE
parser=argparse.ArgumentParser(description="""Daytime server""")
parser.add_argument("-p", "--port", type=int, default=50001)
args=parser.parse_args()
HOST=''
PORT = args.port
# --------------------------------------
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
s.bind((HOST,PORT))
s.listen(1)
# --------------------------------------

peers = []

def usr1(signum, frame):
    print("Signal handler with signal: ", signum)
    global peers
    print ("llista de connexions: ", peers)
    sys.exit(0)

def usr2(signum, frame):
    print("Signal handler with signal: ", signum)
    global peers
    print("Contador de connexions: ", len(peers))
    sys.exit(0)

def term(signum, fram):
    print("Signal handler with signal: ", signum)
    global peers
    print ("llista de connexions: ", peers)
    print("Contador de connexions: ", len(peers))    
    sys.exit(0)

# Fer un fork

pid=os.fork()
if pid !=0:
    #os.wait()
    print("Programa pare: ",os.getpid(), pid)
    print("Lançant el proces fill servidor")
    sys.exit(0)

# proces fill
signal.signal(signal.SIGUSR1,usr1)          #10
signal.signal(signal.SIGUSR2,usr2)          #12
signal.signal(signal.SIGTERM,term)          #15

while True:
    conn, addr = s.accept()
    print("Connected by: ", addr)
    peers.append(addr)
    # fer el popen
    command = "ss -ltn"
    pipeData = Popen(command, stdout=PIPE)
    for line in pipeData.stdout:
        conn.send(line)
    conn.close()


#docker run --rm -h python --name python -v $/pwd):/test -p 55001:55001 -it python /bin/bash

#instal·lar nmap iproute2 ncat