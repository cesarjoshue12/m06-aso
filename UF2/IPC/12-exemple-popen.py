#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# @edt ASIX M06 Curs 2023-2024
# programa.py ruta
# -------------------------------
import sys
import argparse
# fem un from per agafar els metodes Popen, PIPE aixi despres no hem de fer subprocess.PIPE

from subprocess import Popen, PIPE

# ------------------------------
command = "psql -qtA -F',' -h localhost -U postgres training -c \"select * from clientes; \"" 
pipeData = Popen(command, shell=True,stdout=PIPE)
for line in pipeData.stdout:
    print(line.decode("utf-8"),end="")
exit(0)

