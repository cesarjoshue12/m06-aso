#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# @César M06 Curs 2023-2024
# daytime-client.py
# --------------------------------------
import sys,socket
HOST = '' # localhost
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Crea un objecte de tipus socket (socket.AF_INET --> internet, socket.SOCK_STREAM --> TCP)
# s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # Quan acabem el programa deixa utilitzar el port, sino hauriem d'esperar o utilitza un altre port (TIME WAIT)
s.connect((HOST,PORT)) # Per quines inteficies i port ha de escoltar
while True:
    data = s.recv(1024)
    if not data: break
    print("Data: ",repr(data))

s.close()
sys.exit(0)

