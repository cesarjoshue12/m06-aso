#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# @edt ASIX M06 Curs 2023-2024

# ENUNCIAT
# head [-n 5|10|15]  file
# default=10, file o stdin

# -------------------------------
import sys, argparse

parser = argparse.ArgumentParser(description=\
        """Mostrar les N primereslinies """,\
        prog="03-head-args.py",\
        epilog="thats all folks")
parser.add_argument("-n","--nlin",type=int,\
        help="Numero de linies (5,10,15)",dest="nlin",\
        metavar="numlinies",choices=[5,10,15])

parser.add_argument("file",type=str,\
        help="fitxer a processar (stdin)",metavar="file")

args=parser.parse_args()
print(args)
# ------------------------------

MAX=args.nlin
fileIn=open(args.file,"r")
i=0 
for line in fileIn:
    i+=1
    print(line,end="")
    if i==MAX: break
fileIn.close()
exit(0)


