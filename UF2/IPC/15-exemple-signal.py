# /user/bin/python3
#-*- coding: utf-8-*-
# @ Abril 2024
# signal-exemple.py
# ------------------------------
import sys, os, signal

# funcio handler
def myhandler(signum, frame): # Obligatori tota funcio de manipulacio ha de tenir dos arguments
    print("Signal handler with signal: ", signum)
    print("hasta luego lucas!")
    sys.exit(0)

def nodeath(signum, frame):
    print("Signal handler with signal: ", signum)
    print("Tururut! moret tu")

# Assignar un handler al senyal
signal.signal(signal.SIGUSR1,myhandler)         #10 --> Quan s envia un sigterm de user fa la funcio myhandler
signal.signal(signal.SIGUSR2,nodeath)           #12 --> Quan s envia un sigterm de user fa la funcio myhandler
signal.signal(signal.SIGALRM,myhandler)         #14 --> Posar alarma
signal.signal(signal.SIGTERM,signal.SIG_IGN)    #15 --> Ignorar senyal per SIG_ING
signal.signal(signal.SIGINT,signal.SIG_IGN)     #2  --> Ignorar senyal


signal.alarm(60) # posar alarma pasat 60s

print(os.getpid())
while True: # bucle infinit
    pass # no fa res
sys.exit(0)


