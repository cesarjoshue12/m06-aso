# /usr/bin/python3
#-*- coding: utf-8-*-
#
# fork-signal.py segons
# -------------------------------------
# @César Aguilar Méndez
# Abril 2024
# -------------------------------------
import sys, os, signal

def myusr1(signum, frame):
  print("Signal handler with signal: ", signum)
  print("Hola")

def myusr2(signum, frame):
  print("Signal handler with signal:", signum)
  print("Adeu")

# -------------------------------------
print("Hola, començament del program principal")
print("PID pare: ", os.getpid())
pid=os.fork()
if pid !=0:
    #os.wait()
    print("Programa pare: ",os.getpid(), pid)
    print("Lançant el proces fill servidor")
    sys.exit(0)
    
print("Programa fill: ",os.getpid(), pid)
# Assignar un handler al senyal
signal.signal(signal.SIGUSR1,myusr1)          #10
signal.signal(signal.SIGUSR2,myusr2)          #12
while True:
    pass


# això no s'executarà mai
print("Hasta luego Lucas!")
sys.exit(0)


