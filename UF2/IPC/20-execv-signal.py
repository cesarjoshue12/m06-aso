# /usr/bin/python3
#-*- coding: utf-8-*-
#
# signal.py segons
# -------------------------------------
# @César Aguilar Méndez
# Abril 2024
# -------------------------------------
import sys,os
print("Hola, començament del program principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
    #os.wait()
    print("Programa pare: ",os.getpid(), pid)
    sys.exit(0)

# programa fill
#os.execv("/usr/bin/ls", ["/usr/bin/ls", "-la","/"]) # Ahora se este proceso
#os.execl("/usr/bin/ls","/usr/bin/ls","-la","/")
#os.execlp("ls","ls","-la","/")
#os.execvp("uname",["uname","-a"])
#os.execv("/bin/bash",["/bin/bash", "show.sh"])
os.execv("/usr/bin/python3",["/usr/bin/python3","16-signal.py","70"]

print("Hasta luego Lucas!")
sys.exit(0)


