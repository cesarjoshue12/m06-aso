#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# @edt ASIX M06 Curs 2023-2024

# ENUNCIAT
# head [-n 5|10|15]  -v file...
# default=10, file o stdin

# -------------------------------
import sys, argparse

parser = argparse.ArgumentParser(description=\
        """Mostrar les N primereslinies """,\
        prog="03-head-args.py",\
        epilog="thats all folks")
parser.add_argument("-n","--nlin",type=int,\
        help="Numero de linies (5,10,15)",dest="nlin",\
        metavar="numlinies",choices=[5,10,15])

parser.add_argument("file_list",type=str,\
        help="fitxer a processar (stdin)",metavar="file",\
        nargs="*")

parser.add_argument("-v","--verbose",\
        action="store_true",default=False)

args=parser.parse_args()
print(args)

# ------------------------------

MAX=args.nlin

def headFile(fitxer):

 fileIn=open(fitxer,"r")
 i=0 
 for line in fileIn:
  i+=1
  print(line,end="")
  if i==MAX: break
 fileIn.close()


if args.file_list:
 for fileName in args.file_list:
     if args.verbose: print("\n",fileName,40*"-")
  headFile(fileName)

exit(0)


