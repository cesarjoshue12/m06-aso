#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# @César M06 Curs 2023-2024
# daytime-server.py
# ------------------------------------
import sys, socket
HOST = ''
PORT = 50001

# Crear socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
conn, addr = s.accept()
print("Conn:", type(conn), conn) # Variable que es un objecte que representa una connexió
print("Connected by:", addr)


# Popen
from subprocess import Popen, PIPE
command = [ "date" ]
#command = ["lsof"]
pipeData = Popen(command, stdout=PIPE)
for line in pipeData.stdout:
    #print(line)
    conn.send(line)
conn.close()
sys.exit(0)
exit(0)




